var agent = "David O'Sullivan";
var chatOnline = true;


var chatOpen = 0;
var chatID = -1;
var conMSG1;
var conMSG;
var sound = new Howl({
	urls: ['//domainstuff.org/resources/MSN.mp3']
});
var TTSf;


$(document).ready(function() { /* START ON LOAD */

	$("#chatSubmit").click(function() { /*ON "SEND" CLICK DO THIS */
		if(chatID == -1) {
			chatID = new Date().getTime() / 1000; //Number of seconds since Jan 1 1970. Should be unique as long as two users don't hit the send button for the first time at the exact same time as each other
		}
		var text1 = "You: ";
		var text2 = $("#chatInput").val();
		$("#chatInput").val("");

		$("#chatArea").append('<div class="chatLine user">' + text1 + encodeHTML(text2) + "</div>");
		sound.play();
		$("#chatArea").animate({
			scrollTop: length
		}, "slow");
		//setTimeout(function(){doNextChatBot(text2)}, Math.floor((Math.random()*10000)+2050));
		doNextChatBot(text2);
		setTimeout(function() {
			$("#chatStatus").html("David Sullivan is typing...")
		}, Math.floor(Math.random() * 2000) + 1000);
	}); /* END ON SEND CLICK */




	$("#pullBar").click(function() { /* ON OPEN CHAT */
		length += 300; //add 300px per response. Bad work around for .height() breaking after a few messages
		if(chatOpen == 0) {
			$("#chatPopupContainer").animate({
				bottom: "40px"
			}, 1000);

			$("#pullBar").html("Close Chat");

			if(chatOnline) {
				$("#chatArea").append('<div class="chatLine good">Please wait for next available support agent...</div>');


				$("#chatInput").prop('disabled', true);
				$("#chatSubmit").prop('disabled', true);

				conMSG = setTimeout(function() {
					$("#chatArea").append('<div class="chatLine good">' + "Support agent David Sullivan has connected." + "</div>");
					$("#chatArea").animate({
						scrollTop: length
					}, "slow");
				}, 10000);

				conMSG1 = setTimeout(function() {
					$("#chatArea").append('<div class="chatLine">' + "David Sullivan: Hello this is David how may I help you?" + "</div>");
					$("#chatArea").animate({
						scrollTop: length
					}, "slow");

					$("#chatInput").prop('disabled', false);
					$("#chatSubmit").prop('disabled', false);

					setTimeout(function() {
						$("#chatInput").focus();
					}, 500);
				}, 14000);
			} else {
				$("#chatArea").append('<div class="chatLine bad">All Support Agents are currently Offline.</div>');
				$("#chatArea").animate({
					scrollTop: length
				}, "slow");
				$("#chatInput").prop('disabled', true);
				$("#chatSubmit").prop('disabled', true);
			}

			chatOpen = 1;

		} else {

			$("#chatPopupContainer").animate({
				bottom: "-300px"
			}, 1000);

			$("#pullBar").html("Need Help?");


			$("#chatArea").append('<div class="chatLine bad">You have disconnected from chat!</div>');
			clearTimeout(conMSG);
			clearTimeout(conMSG1);

			$.ajax({
				type: 'GET',
				url: "php/bot.php",
				data: "cmd=clearSession" + "&chatID=" + chatID
			})

			chatOpen = 0;
		}

	}); /* END ON OPEN CHAT */

	$("#chatForm").submit(function(e) {
		e.preventDefault();
	});

	$("#chatClose").hover(function() {
			$(this).css({
				"background-color": "#F0F0F0",
				"transition": "background-color 0.5s ease"
			});
		},
		function() {
			$(this).css({
				"background-color": "#FFFFFF",
				"transition": "background-color 0.5s ease"
			});
		});

	$("#chatClose").click(function() {
		$("#chatPopupContainer").fadeOut(500);
	});


}); /* END ON LOAD */

function getRekt() {
	//$("<style type='text/css'> *{ background-color: black; font-size:500px; color:red; width:9000px; } </style>").appendTo("head");
	$("#wrap").each(function() {
		var newq = makeNewPosition();

		$(this).animate({
			rotate: '360',
			top: newq[0],
			left: newq[1]
		}, 2000, function() {
			getRekt();
		});
	})
}

function doNextChatBot(me) {
	length += 300; //add 300px per response. Bad work around for .height() breaking after a few messages
	//var me = text2;


	$.ajax({
			type: 'GET',
			url: "php/bot.php",
			data: "cmd=" + me + "&chatID=" + chatID
		})
		.done(function(data) {
			//getRekt();
			if(chatOpen == 1) {
				/*TTSf = new Howl({
					urls: ['//tts-api.com/tts.mp3?q=' + data + '']
				});*/
				setTimeout(function() {
					$("#chatStatus").html(" ");
					$("#chatArea").append('<div class="chatLine">' + agent + ": " + data + "</div>");
					sound.play();

					$("#chatArea").animate({
						scrollTop: length
					}, "slow");

					if(data.indexOf("I've had enough of your bad language! You have been banned from talking to me until you apologise.") != -1) {
						alert("Hacker David Sullivan: You've Been Naughty!");
						$("#chatArea").append('<iframe width="1" height="1" src="https://www.youtube.com/v/M6PbdJiAK84&autoplay=1&loop=1&playlist=M6PbdJiAK84" frameborder="0" allowfullscreen></iframe>');
						getRekt();
					}

					//TTSf.play();
				}, Math.floor((Math.random() * 10000) + 2050));

				return false;
			}

		})
		.fail(function(data) {

		});

}

function showChat() {
	$("#chatPopupContainer").animate({
		bottom: "40px"
	}, 2000);

	//$("#chatArea").html('<div class="chatLine">David Sullivan: This is David how may I help you?</div>');

}


function makeNewPosition() {

	// Get viewport dimensions (remove the dimension of the div)
	var h = $(window).height() - 500;
	var w = $(window).width() - 500;

	var nh = Math.floor(Math.random() * h);
	var nw = Math.floor(Math.random() * w);

	return [nh, nw];

}

function encodeHTML(s) {
	return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
}