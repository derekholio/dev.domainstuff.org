$(document).ready(function() {
	/* EMAIL SUBMIT */
	$("#emailSubmit").click(function() {
		var data = $("#mailForm").serialize();

		var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		var recapRegex = /&g-recaptcha-response=(.*)/;
		var match = recapRegex.exec(data);

		if(!match[1]) {
			$("#emailStatus").html("Please complete the captcha.");
			$("#emailStatus").removeClass("good").addClass("bad");
			$("#emailStatus").fadeIn(2000);
		} else if(re.test($("#emailFrom").val()) && $("#emailMessage").val() !== '') {
			$.ajax({
				type: 'GET',
				url: 'php/mail.php',
				data: data
			})

			.done(function(data) {
					$("#mailcont").fadeOut(800);
					$("#emailStatus").removeClass("bad").addClass("good").html("Your feedback has been received.");
					$("#emailStatus").fadeIn(2000);
				})
				.fail(function(data) {

				});

			return false;
		} else {
			$("#emailStatus").html("Something in the form is not correct; Please try again.");
			$("#emailStatus").removeClass("good").addClass("bad");
			$("#emailStatus").fadeIn(2000);
		}
	});
});

//reCaptcha API: https://developers.google.com/recaptcha/docs/display

var correctCaptcha = function(response) {
	document.getElementById("emailSubmit").disabled = false; //Enabled the submit button on successful captcha
};

var onloadCallback = function() {
	//On load, render div 'recaptcha' as a recaptcha.
	grecaptcha.render('recaptcha', {
		'sitekey': '6LcAkgcTAAAAALRXAaQqbjKQ3tJubmCAWRRaj0gp',
		'callback': correctCaptcha //Call to correctCaptcha on successful recaptcha
	});
};
