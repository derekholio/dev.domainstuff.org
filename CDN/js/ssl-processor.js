function checkSSL(domain) {

	var ssl_html;
	var ssl_checked_hostname, ssl_name, ssl_issuer, ssl_is_valid, ssl_error;
	var ssl_is_vulnerable_heartbleed;
	var ssl_protos, ssl_ciphers, ssl_strict_transport_security, ssl_warnings;

	$.ajax({
			type: 'GET',
			url: 'php/sslchecker.php',
			data: 'domain=' + domain.domainName,
			dataType: 'json'
		})
		.done(function(json_data) {
			//console.log(json_data);
			ssl_error = json_data.error;
			if(ssl_error) {
				if(String(ssl_error).match(/Failed\sto\sconnect/i)) {
					ssl_error = "Failed to connect to the provided domain";
				}
				ssl_html = '<div class="containerContent bad"><b>Valid SSL: </b>False (' + ssl_error + ')</div>';
			} else {
				ssl_is_valid = (json_data.data.connection.validation.status == "success") ? true : false;
				if(!ssl_is_valid) {
					ssl_error = json_data.data.connection.validation.error;
				}
				ssl_warnings = json_data.data.connection.warning;
				ssl_checked_hostname = json_data.data.connection.checked_hostname;
				ssl_name = json_data.data.connection.chain[0].name;
				ssl_issuer = json_data.data.connection.chain[0].issuer;
				ssl_is_vulnerable_heartbleed = (json_data.data.connection.heartbleed == "not_vulnerable") ? false : true;
				ssl_protos = json_data.data.connection.protocols;
				ssl_ciphers = json_data.data.connection.supported_ciphersuites;
				ssl_strict_transport_security = (json_data.data.connection.strict_transport_security == "not set") ? false : true;

				ssl_html = '<div class="containerContent"><b>Checked Hostname: </b>' + ssl_checked_hostname + '</div>';
				ssl_html += '<div class="containerContent"><b>SSL Name: </b>' + ssl_name + '</div>';
				ssl_html += '<div class="containerContent"><b>SSL Issuer: </b>' + ssl_issuer + '</div>';
				if(ssl_is_valid) {
					ssl_html += '<div class="containerContent good"><b>Valid SSL: </b>True</div>';
				} else {
					ssl_html += '<div class="containerContent bad"><b>Valid SSL: </b>False</div>';
				}
				if(ssl_warnings) {
					ssl_html += '<div class="containerContent warning"><p class="contentTitle">SSL Warnings</p>' + ssl_warnings.join('<br />') + '</div>';
				}
				if(ssl_is_vulnerable_heartbleed) {
					ssl_html += '<div class="containerContent bad"><b>Heartbleed Vulnerability: </b>VULNERABLE</div>';
				} else {
					ssl_html += '<div class="containerContent good"><b>Heartbleed Vulnerability: </b>Not vulnerable</div>';
				}
				ssl_html += '<div class="containerContent"><p class="contentTitle">Supported Ciphers</p>' + ssl_ciphers.join('<br />') + '</div>';

				var protos_html = "";
				var keys = [];
				for(var key in ssl_protos) {
					if(ssl_protos.hasOwnProperty(key)) {
						keys.push(key);
					}
				}
				keys.forEach(function(element, index, array) {
					var proto = element;
					var enabled = ssl_protos[element];
					if(enabled) {
						protos_html += '<b>' + proto + '</b>: Enabled<br/>';
					} else {
						protos_html += '<b>' + proto + '</b>: Disabled<br/>';
					}
				});
				ssl_html += '<div class="containerContent"><p class="contentTitle">Supported Protocols</p>' + protos_html + '</div>';
			}

			$("#SSLChecker").html(ssl_html);
		})
		.fail(function(data) {
			console.log('Call to SSLChecker failed.');
			console.log(data);
			$("#SSL").html('<div class="containerEntry"><div class="containerContent bad">Failed to check the SSL</div></div>');
		});
}
