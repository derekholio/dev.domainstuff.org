var quoteCount=0;
var quotes = [
	"For all my World of Tanks needs, I choose Managed.com",
	"The support crew at Managed.com is the best!",
	"I love Managed.com!",
	"That's just it, Managed.com is the best",
	"Take it from David's Mom's son, Managed.com is the best!",
	"If you haffina host a website, choose Managed.com",
	"From solving your Drupal errors to cutting your roast beef, Managed.com does it all!",
	"Managed.com roars true like the engine of a M4 Sherman"
];

$(document).ready(function() {

	var width = $(window).width();
	if(width < 1024){
		$("#contentright").remove();
		return;
	}

	if(getCookie("ads") == "no") {
		$("#contentright").hide();
	}

	/* HIDE ADS 4 FREE! F-O-R-4-F-R-E-E*/
	$("#hideAds").click(function() {
		$("#contentright").fadeOut("slow");
		setCookie("ads", "no");
	});

	doAd();
	setInterval(doAd, 8000);

	function doAd() {
		var $adSpace = $("#adContainer p");
		$adSpace.fadeOut(1000, function() {
			$(this).html("\"" + quotes[quoteCount] + "\"<br>- David Sullivan").fadeIn(500);
		});

		quoteCount += 1;
		if(quoteCount > quotes.length - 1) quoteCount = 0;
	}

	function setCookie(cname, cvalue) {
		var d = new Date();
		d.setTime(d.getTime() + (1 * 1 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while(c.charAt(0) == ' ') c = c.substring(1);
			if(c.indexOf(name) == 0) return c.substring(name.length, c.length);
		}
		return "";
	}

	function deleteCookie(name) {
		document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
});
