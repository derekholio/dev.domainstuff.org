$(document).ready(function () {

    $('#accordion').accordion({
        collapsible: true,
        heightStyle: "content",
        beforeActivate: function (event, ui) {
            // The accordion believes a panel is being opened
            if (ui.newHeader[0]) {
                var currHeader = ui.newHeader;
                var currContent = currHeader.next('.ui-accordion-content');
                // The accordion believes a panel is being closed
            } else {
                var currHeader = ui.oldHeader;
                var currContent = currHeader.next('.ui-accordion-content');
            }
            // Since we've changed the default behavior, this detects the actual status
            var isPanelSelected = currHeader.attr('aria-selected') == 'true';

            // Toggle the panel's header
            currHeader.toggleClass('ui-corner-all', isPanelSelected).toggleClass('accordion-header-active ui-state-active ui-corner-top', !isPanelSelected).attr('aria-selected', ((!isPanelSelected).toString()));

            // Toggle the panel's icon
            currHeader.children('.ui-icon').toggleClass('ui-icon-triangle-1-e', isPanelSelected).toggleClass('ui-icon-triangle-1-s', !isPanelSelected);

            // Toggle the panel's content
            currContent.toggleClass('accordion-content-active', !isPanelSelected)
            if (isPanelSelected) { currContent.slideUp(); } else { currContent.slideDown(); }

            return false; // Cancels the default action
        }
    });
	
	
	//"CMS" tab sorting
	 var $list = $("#list");

	  $list.children().detach().sort(function(a, b) {
		return $(a).text().localeCompare($(b).text());
	  }).appendTo($list);


	  $(".containerEntry").each(function () {
	      var a = 0;

	      $(this).find(".mono").each(function() {
	          var b = $(this).actual('width', { clone: true });
	          if (b > a) { a = b+20; }
	      });

	      $(this).find(".mono").css("width", a);
	  });




	/*
	//Stupid CMS table
	var $table = $("#CMS").stupidtable();
	var $th_to_sort = $table.find("thead th").eq(0);
	$th_to_sort.stupidsort();//sort by Category asc

	*/

});


function doError(error, time) {
    if (time == 0) time = 9000000;
    $("#error").fadeIn("fast");
    $("#error").html(error);
    $("#error").animate("shake");
    $("#error").delay(time).fadeOut("slow");
}

