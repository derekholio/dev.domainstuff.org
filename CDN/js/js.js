var logging = false;
if(window.location.hostname == "dev.domainstuff.org"){
	logging = true;
}

function logMessage(msg){
	if(logging){
		console.log(msg);
	}
}

//Domain name object
function Domain(domainName) {
	this.domainName = domainName;
	var IP = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(domainName);
	var isDomain;
	var validDomain = !(domainName == '' || /^(?:[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+)$/.test(domainName) == false);
	var validIP = checkValidIP(domainName);
	this.isValidDomain = function() {
		return validDomain;
	}
	this.isValidIP = function() {
		return validIP;
	}
	this.getDomainName = function() {
		return domainName;
	}
	this.isIP = function() {
		return IP;
	}
}

$(document).ready(function() {

	$("#tabs").tabs(); //Create tabs

	/* HIDE DOMAIN NAME AND CONTENT IF NO DOMAIN SET*/
	var domTest = location.pathname.split('/');
	var urlDomainName = location.pathname.split('/')[1] ? location.pathname.split('/')[1] : '';
	var domain = new Domain(urlDomainName);

	if(!domain.isValidDomain() && !domain.isValidIP()) {
		$("#content").html('');
		$("#domainName").html("");
		$("#geoLoc").html('');
		if(domain.domainName != '') {
			doError("Please enter a valid domain or IP address!", 3000);
		}
	} else {
		$("#domainName").html('Showing information for: <a target="_blank" href="http://' + domain.domainName + '">' + domain.domainName.toUpperCase() + '</a>');
	}

	/* AJAX CALLS & afterLoad calls */
	//ajaxThis(CMD, DOMAIN, FILE, JQ SELECTOR)
	if(domain.isIP()) {
		if(domain.isValidIP()) {
			checkFirewall(domain.domainName);//Get open ports
			ajaxThis("GetGeoIP", domain.domainName, "php/geoip.php", "#geoLoc");
			//Reverse DNS
			ip_reverseDNS(domain.domainName);
			//Blacklists
			ip_blacklists(domain.domainName);

			$("#DNS-TAB").remove();
			$("#CMS-TAB").remove();
			$("#SNAPSHOT-TAB").remove();
			$("#REDIRECT-TAB").remove();
			$("#DNSPROP-TAB").remove();
			$("#SSL-TAB").remove();

			$("#tabs").tabs({
				active: 1
			}); //Set reverse DNS to the active tab, since the orginial first tab is gone
		}
	} else if(domain.isValidDomain()) {
		getDNSRecords(domain.domainName);
		ajaxThis("", domain.domainName, "WhatsMyDNSDriver.py", "#WhatsMyDNS"); //Get DNS prop
		checkFirewall(domain.domainName);//Get open ports
		ajaxThis("CheckRedirect", domain.domainName, "RedirectCheckDriver.py", "#RedirectChecker"); //Scan URL for directs
		ajaxThis("CheckCMS", domain.domainName, "CMSDriver.py", "#CMSCheck");
		ajaxThis("", domain.domainName, "php/render.php", "#SnapshotContent");
		ajaxThis("GetGeoIP", domain.domainName, "php/geoip.php", "#geoLoc");
		checkSSL(domain);
	}

	sortList();

});

function sortList() {
	//"CMS" tab sorting
	$(".list").each(function() {
		$(this).children().detach().sort(function(a, b) {
			return $(a).text().localeCompare($(b).text());
		}).appendTo($(this));
	});
}

function ajaxThis(cmd, domain, file, selector) {
	$.ajax({
		type: 'GET',
		url: file,
		data: "cmd=" + cmd + "&domain=" + domain
	})
	.done(function(data) {
			$(selector).html(data);
			sortList();
		})
		.fail(function(data) {
			logMessage("AJAX FAILED OH NO qq rip " + data);
		});

	return false;
}

function checkFirewall(domain){
	logMessage("Checking for open ports.");
	$.ajax({
		type: 'GET',
		url: 'php/firewall.php',
		data: 'domain='+domain,
		dataType: 'json'
	})
	.done(function(data){
		if(data.error){
			logMessage("Firewall error: " + data.error);
			portscan_html = '<div class="containerContent bad">Failed to call port scanner: ' + data.error + '</div>';
			$("#PortScan").html(portscan_html);
		}else{
			portscan_html = "";
			logMessage("Ports: " + data);
			data.forEach(function(element, index, array){
				var port = element.Port;
				var service = element.Service;
				var status = element.Status;
				if(status){
					logMessage("Open port: " + element.Port);
					portscan_html += '<div class="containerContent good"><b>'+port+' ('+service+')'+'</b>: Open</div>';
				}else{
					logMessage("Closed port: " + element.Port);
					portscan_html += '<div class="containerContent bad"><b>'+port+' ('+service+')'+'</b>: Closed</div>';
				}
			});
			$("#PortScan").html(portscan_html);
		}
	})
	.fail(function(data){
		logMessage("Failed to called firewall scanner.");
		portscan_html = '<div class="containerContent bad">Failed to call port scanner: (HTTP Error)</div>';
		$("#PortScan").html(portscan_html);
	})
}

//Passed an IP address, get the reverse DNS and update RDNS
function ip_reverseDNS(ipaddr) {
	$.ajax({
			type: 'GET',
			url: 'php/dnsrecords.php',
			data: 'domain=' + ipaddr,
			dataType: 'json'
		})
		.done(function(data) {
			var rdns_html = "";
			if(data.PTR == "None") {
				rdns_html = '<div class="containerEntry"><div class="containerContent bad"><b>' + ipaddr + '</b>: No PTR</div></div>';
			} else {
				rdns_html = '<div class="containerEntry"><div class="containerContent good"><b>' + ipaddr + '</b>: ' + data.PTR + '</div></div>';
			}
			$("#RDNS").html(rdns_html);
		})
}

//Passed an IP address, check the blacklists status and update Blacklists
function ip_blacklists(ipaddr) {
	$.ajax({
			type: 'GET',
			url: 'php/blacklists.php',
			data: 'ip=' + ipaddr,
			dataType: 'json'
		})
		.done(function(data) {
			var blacklists_html = "";
			blacklists_html += '<div class="containerEntry">';
			data.forEach(function(element, index, array) {
				if(element.Status == 0) {
					blacklists_html += '<div class="containerContent good"><b>' + element.RBL + '</b>: Not listed!</div>';
				} else {
					blacklists_html += '<div class="containerContent bad"><b>' + element.RBL + '</b>: Listed with a status of ' + element.Status + '</div>';
				}
			});
			blacklists_html += '</div>';
			$("#Blacklists").html(blacklists_html);
		})
}

//Given a domain name, retrieve all DNS records, as well as reverse DNS and blacklists
function getDNSRecords(domain) {
	logMessage("Getting DNS records");
	$.ajax({
			type: 'GET',
			url: '/php/dnsrecords.php',
			data: 'domain=' + domain,
			dataType: 'json',
			//timeout: 10000
		})
		.done(function(data) {
			logMessage(data);
			var dns_error = data.error;
			if(dns_error) {
				var err_html = '<div class="containerEntry"><div class="containerContent bad">' + dns_error + '</div></div>';
				//console.log(err_html);
				$("#DNS").html(err_html);
				$("#RDNS").html(err_html);
				$("#Blacklists").html(err_html);
			} else {
				var dns_html;
				var a_records = data.A;
				var aaaa_records = data.AAAA;
				var mx_records = data.MX;
				var ns_records = data.NS;
				var soa_records = data.SOA;
				var txt_records = data.TXT;
				var rDNS_records = [];
				var bl_records = [];

				dns_html = '<div class="containerEntry list">';

				if(a_records && a_records.length > 0) {
					a_records.forEach(function(element, index, array) {
						logMessage(array);
						logMessage(element);
						dns_html += '<div class="containerContent good"><b>A: </b>' + element.A + '</div>'
						rDNS_records.push(["[A]", element.A, element.PTR]);
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>A: </b>No records</div>';
				}

				if(aaaa_records && aaaa_records.length > 0) {
					aaaa_records.forEach(function(element, index, array) {
						dns_html += '<div class="containerContent good"><b>AAAA: </b>' + element + '</div>';
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>AAAA: </b>No records</div>';
				}

				if(mx_records && mx_records.length > 0) {
					//Get MX records
					mx_records.forEach(function(element, index, array) {
						var priority = element['Priority'];
						var rec = element['Record'];
						dns_html += '<div class="containerContent good"><b>MX: </b>' + priority + ':' + rec + ' ('+element['A']+')</div>';
						rDNS_records.push(["[MX]", element['A'], element['PTR']]);
						bl_records.push(element['A']);
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>MX: </b>No records</div>';
					$("#RDNS").html('<div class="containerContent warning">No records to query.</div>');
					$("#Blacklists").html('<div class="containerContent warning">No records to query.</div>');
				}

				logMessage('rDNS');
				if(rDNS_records && rDNS_records.length > 0) {
					logMessage(rDNS_records.length);
					var rdns_html = "<div class='containerEntry'>";
					rDNS_records.forEach(function(element, index, array) {
						rdns_html += '<div class="containerContent"><b>' + element[0] + ' ' + element[1] + '</b>: ' + element[2] + '</div>';
					})
					rdns_html += '</div>';
					$("#RDNS").html(rdns_html);
				} else {
					var rdns_html = "<div class='containerEntry'>"
					rdns_html += '<div class="containerContent">No records to query.</div>'
					rdns_html += '</div>';
					$("#RDNS").html(rdns_html);
				}

				if(bl_records && bl_records.length > 0) {
					var calls = 0;
					var bl_html = '<div class="containerEntry">';
					bl_records.forEach(function(element, index, array) {
						$.ajax({
								type: 'GET',
								url: '/php/blacklists.php',
								data: 'ip=' + element,
								dataType: 'json'
							})
							.done(function(data) {
								if(calls != 0) {
									bl_html += '<hr class="separator">';
								}
								calls++;
								data.forEach(function(ele, i, arr) {
									if(ele.Status == 0) {
										bl_html += '<div class="containerContent good"><b>' + ele.RBL + '</b>: Not listed.</div>';
									} else {
										bl_html += '<div class="containerContent bad"><b>' + ele.RBL + '</b>: Listed!</div>';
									}
								});
								bl_html += '</div>';
								$("#Blacklists").html(bl_html);
							})
							.fail(function(data) {
								logMessage(data);
							})
					});

				} else {
					var bl_html = '<div class="containerEntry">';
					bl_html += '<div class="containerContent">No records to query.</div>';
					bl_html += '</div>';
					$("#Blacklists").html(bl_html);
				}

				if(ns_records && ns_records.length > 0) {
					ns_records.forEach(function(element, index, array) {
						dns_html += '<div class="containerContent good"><b>NS: </b>' + element.NS + ' (' + element.A + ')</div>';
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>NS: </b>No records</div>';
				}

				if(soa_records && soa_records.length > 0) {
					soa_records.forEach(function(element, index, array) {
						var expiry = element['Expiry'];
						var ttl = element['Minimum TTL'];
						var auth = element['Record'];
						var refresh = element['Refresh'];
						var responsible = element['Responsible'];
						var retry = element['Retry'];
						var serial = element['Serial'];
						dns_html += '<div class="containerContent good"><b><div class="contentTitle">SOA</div></b>';
						dns_html += 'Authority: ' + auth + '<br />';
						dns_html += 'Responsible: ' + responsible + '<br />';
						dns_html += 'Serial: ' + serial + '<br />';
						dns_html += 'Refresh: ' + retry + '<br />';
						dns_html += 'Expiry: ' + expiry + '<br />';
						dns_html += 'TTL: ' + ttl + '<br />';
						dns_html += '</div>';
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>NS: </b>No records</div>';
				}

				if(txt_records && txt_records.length > 0) {
					txt_records.forEach(function(element, index, array) {
						var record = element;
						if(record.match(/^v=spf1/)) {
							dns_html += '<div class="containerContent good"><b>SPF: </b>' + record + '</div>';
						} else {
							dns_html += '<div class="containerContent good"><b>TXT: </b>' + record + '</div>';
						}
					})
				} else {
					dns_html += '<div class="containerContent bad"><b>SPF/TXT: </b>No records</div>';
				}

				dns_html += '</div>';
				//console.log(dns_html);
				$("#DNS").html(dns_html);
			}
		})
		.fail(function(data) {
			logMessage('Failed');
			logMessage(data);
			if(data.statusText == "timeout"){
				logMessage('Timeout pulling DNS.');
			}
			return false;
		})
		.always(function() {
			logMessage("AJAX done.");
		})
}

//Display an error pop up on the page
function doError(error, time) {
	if(time == 0) time = 9000000;
	$("#error").fadeIn("fast");
	$("#error").html(error);
	$("#error").animate("shake");
	$("#error").delay(time).fadeOut("slow");
}

//Verify that the passed IP address is valid
function checkValidIP(ip) {
	var ipRe = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/
	var match = ipRe.exec(ip);
	if(match != null && (match[1] > 255 || match[2] > 255 || match[3] > 255 || match[4] > 255)) {
		isValidIP = false;
	} else if(match == null) {
		isValidIP = false;
	} else {
		isValidIP = true;
	}

	return isValidIP;
}

//Validate that the input is correct
function validate() {
	val = $("#domainInput").val();
	var valDomain = new Domain(val);
	logMessage(valDomain.isValidIP());
	logMessage(valDomain.isValidDomain());

	if(!valDomain.isValidIP() && !valDomain.isValidDomain()) {
		doError("Invalid!", 2500);
		return false;
	} else {
		$("#domainName").html('');
		$("#geoLoc").html('');
		$("#content").html('Working...<br><img src="//cdn.'+window.location.hostname+'/img/loading.gif">');
		return true;
	}
}
