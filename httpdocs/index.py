import cgi
import os
import re

import WHOISDriver

print "Content-Type: text/html\r\n"


def get_file_contents(path):
    cur_path = os.path.dirname(os.path.realpath(__file__))
    html_file = open(cur_path + '\\' + path, 'r')
    html = html_file.read()
    html_file.close()
    return str(html)


def sort_multi(query_answers):
    """Sort multiple items into an ordered list"""
    query_list = []
    for query_answer in query_answers:
        query_list.append(query_answer)
    query_list.sort()
    return query_list


def print_html(html, domain_error="", whois_content="", query_domain=""):
    if query_domain != '':
        html = html.replace("%%DESCRIPTION", "DomainStuff: Domain overview for %s" % query_domain)
        html = html.replace("%%QUERY_DOMAIN_TITLE", "%s -" % query_domain)
    else:
        html = html.replace("%%DESCRIPTION",
                            "Provides a domain over view of a domain's DNS records, web application, and more.")
        html = html.replace("%%QUERY_DOMAIN_TITLE", "")

    html = html.replace("%%DOMAINERROR", domain_error)
    html = html.replace("%%WHOISCONTENT", whois_content)
    html = html.replace("%%QUERY_DOMAIN", query_domain)

    print html


def main(html):
    domain_error, whois_content, query_domain = ("", "", "")  # Init values

    form = cgi.FieldStorage()
    if form.getvalue('d'):  # domain was specified
        query_domain = form.getvalue('d')
        m = re.search('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})', query_domain)
        if m is not None:
            if int(m.group(1)) > 255 or int(m.group(2)) > 255 or int(m.group(3)) > 255 or int(m.group(4)) > 255:
                domain_error = '<script>doError("Invalid IP address!", 10000);</script>'
                print_html(html, domain_error=domain_error)
                exit()
            is_ip = True
        else:
            is_ip = False
            m = re.search('^(?:[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+)$', query_domain)
            if m is None:
                print_html(html, domain_error="", query_domain="")
                exit()

        if not is_ip:
            # Strip protocol if given
            m = re.search('^(http://|https://)(.*)', query_domain)
            if m is not None:
                query_domain = m.group(2)
            # Strip www
            m = re.search('^www\.(.*)', query_domain)
            if m is not None:
                query_domain = m.group(1)

    else:  # index
        print_html(html)
        exit()

    # Open WHOIS
    whois_content = '<div class="containerContent specText more">' \
                    '<p class="contentTitle">WHOIS</p>' \
                    + WHOISDriver.get_whois(query_domain) + '</div>'
    # Close WHOIS

    print_html(html, domain_error, whois_content, query_domain)


if __name__ == '__main__':
    # Open html File
    html = get_file_contents('index.py.html')
    # Done with file
    try:
        html = html.replace("%%REQUESTDOMAIN", os.environ["HTTP_HOST"])
        main(html)
    except Exception as e:
        domain_error = '<script>doError("An exception occurred that prevented your request from completing. ' \
                       'This exception has been logged.", 10000);</script>'
        print_html(html, domain_error=domain_error)
else:
    pass
