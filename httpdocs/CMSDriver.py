import cgi
import os

import requests.exceptions
import wappalyzer


def analyze_url(url):
    try:
        analyzer = wappalyzer.Wappalyzer()
        analyzed = analyzer.analyze('http://' + url)
        if len(analyzed) == 0:
            analyzed = None

        return analyzed  # Dictionary of found apps
    except requests.exceptions.ReadTimeout:
        return "Timeout expired."
    except:
        return None


def process_url(query_domain):
    cms_content = ""
    found_apps = analyze_url(query_domain)
    if found_apps is None:
        cms_content += '<div class="containerContent">No apps detected</div>'
    elif found_apps == "Timeout expired.":
        cms_content += '<div class="containerContent bad">Timeout exceeded; Aborting CMS checks.</div>'
    else:
        for app in found_apps:
            category = str(found_apps[app]['categories'][0])
            version = str(found_apps[app]['version']).strip()
            app = str(app)
            if len(category) < 4:  # Everything shorter than 4 is an acronym
                category = category.upper()
            else:
                category = category.title()
            temp = ""
            temp += '<div class="containerContent"><p class="contentTitle">%s </p> ' % category
            temp += '<p class="CMS"><img src="{0}" height="18" width="18" alt="{1}" class="CMSImg">{1}'.format(
                    get_image_url(app), app)
            if found_apps[app]['version'] != '':
                temp += " (v%s)" % version
            temp += '</p>'
            temp += '</div>'
            cms_content += temp
    print cms_content


def get_image_url(app):
    try:
        current_domain = os.environ["HTTP_HOST"]
    except KeyError:  # local request
        current_domain = "dev.domainstuff.org"
    url = "cdn.{0}/icons/{1}.png".format(current_domain, app.replace(" ", "%20"))
    req = requests.request("GET", "http://" + url)
    if req.status_code == 404:
        url = "cdn.{0}/icons/{1}.svg".format(current_domain, app.replace(" ", "%20"))
    return str("//" + url)


def main():
    form = cgi.FieldStorage()
    if form.getvalue('cmd') == "CheckCMS":
        print "Content-Type: text/html\r\n"
        process_url(form.getvalue('domain'))
    else:
        process_url('derekholio.com')


if __name__ == '__main__':
    main()
else:
    pass
