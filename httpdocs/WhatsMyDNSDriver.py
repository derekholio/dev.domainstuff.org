import cgi

import dns.resolver

print "Content-Type: text/html\r\n"


# Google (US): 8.8.8.8, 8.8.4.4
# Level3 (US): 209.44.0.3, 209.244.0.4
# OpenDNS (US): 208.67.222.222, 208.67.220.220
# DNS.WATCH (Germany): 84.200.69.80, 84.200.70.40
# OpenNIC (AU): 103.25.56.238
# AAISP (UK): 217.169.20.20, 217.169.20.21
# Orange (France): 194.158.122.10, 194.158.122.15
# Xtra (New Zealand): 219.89.127.104
# Yandex (RU): 77.88.8.8, 77.88.8.1

def main():
    query_domain = cgi.FieldStorage().getvalue('domain')

    dns_servers = {'Google (US)': '8.8.8.8', 'Level3 (US)': '4.2.2.1', 'OpenDNS (US)': '208.67.222.222',
                   'DNS.WATCH (GER)': '84.200.69.80', 'OpenNIC (AU)': '103.25.56.238',
                   'Orange (FR)': '90.102.162.205', 'Xtra (NZ)': '219.89.127.104', 'Yandex (RU)': '77.88.8.8'}

    resolver = dns.resolver.Resolver()
    resolver.lifetime = 5
    resolver.timeout = 5

    for key in dns_servers:
        print "<div class='containerContent'><p class='contentTitle'> %s</p>" % key
        resolver.nameservers = [dns_servers[key]]
        try:
            answers = resolver.query(query_domain, 'A')
            for ans in answers:
                print str(ans) + '<br />'
        except dns.resolver.Timeout:
            print "Time out expired<br />"
        except dns.resolver.NXDOMAIN:
            print "Non-existant domain<br />"
        except dns.resolver.YXDOMAIN:
            print "Domain too long<br />"
        except dns.resolver.NoAnswer:
            print "No answer<br />"
        except dns.resolver.NoNameservers:
            print "No name servers available<br />"
        print '</div>'


if __name__ == '__main__':
    main()
else:
    pass
