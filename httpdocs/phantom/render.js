var fs = require('fs');
var path = 'phantom-output.txt';

var system = require('system');
var args = system.args;
var domain = args[1];
var page = require('webpage').create();
page.viewportSize = {
	width: 1600,
	height: 900
};
console.log('Opening site: ' + domain);
page.open('http://' + domain, function(status) {
	if (status == "fail") {
		phantom.exit(1);
	}
	console.log(status);
	console.log('Rendering...');
	var saveto = '../screenshot/' + domain + '.png';
	console.log(saveto);
	page.render(saveto);
	phantom.exit();
});
