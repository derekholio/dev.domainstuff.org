// Give the estimated location based on the IP address.
var system = require('system');

function getLoc(data) {
	var loc = data.city;
	if (data.region.length > 0)
		loc = loc + ', ' + data.region;
	if (data.country.length > 0)
		loc = loc + ', ' + data.country;
	if (data.org.length > 0) {
		var org = data.org.replace(/AS\d{1,}\s/g, "");
		loc = loc + ' (' + org + ')';
	}
	return loc;
}

if (system.args.length == 2) {
	var webpage = require('webpage');
	var page = webpage.create();

	var host = system.args[1];
	//console.log(host);
	page.open('http://ipinfo.io/' + host + '/json', function(status) {
		//console.log(status);
		var jsonSource = page.plainText;
		//console.log(jsonSource);
		var ret = JSON.parse(jsonSource);
		console.log('Estimated location: ' + getLoc(ret));
		phantom.exit();
	});
} else {
	console.log('Invalid number of arguments!');
	phantom.exit();
}
