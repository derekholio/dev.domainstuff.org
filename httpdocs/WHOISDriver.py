import cgi
import re

import pythonwhois


def get_whois(query_domain, html=True):
    m = re.search(
            '([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.)?([a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9]\.[a-zA-Z]{2,})$',
            query_domain)
    if m is not None:  # If subdomain, use only the base domain
        query_domain = m.group(2)
    try:
        response = pythonwhois.get_whois(query_domain)
        raw_response = response.get('raw')
        content = ""
        temp = str(raw_response)

        # Escape special characters
        temp = cgi.escape(temp)

        # remove excess characters from output
        if html:
            temp = (temp.replace('\\n', '<br />'))
            temp = (temp.replace("', u'", "<br />"))

        temp = (temp.replace('[u\'', ''))
        temp = (temp.replace("']", ""))
        temp = (temp.replace("\\'", "\'"))
        temp = (temp.replace("[u\"", ""))
        temp = (temp.replace("\"]", ""))
        temp = (temp.replace("\", u' ", ""))
        return content + temp
    except pythonwhois.shared.WhoisException:
        return 'Failed to get WHOIS data'
    except Exception as e:
        return 'An exeception occurred while retrieving the WHOIS for %s (%s)' % (query_domain, e.message)


def main():
    ret = get_whois('derekholio.com', False)
    print ret


if __name__ == '__main__':
    main()
else:
    pass
