<?php

if(!isset($_GET['ip'])){
    die("No IP address passed.");
}

require('phpdns-1.05/dns.inc.php');

function getRecords($domain){
	$server = "208.88.77.77";
	$lookup = new DNSQuery($server);
	$lookup->timeout = 3;
	$result = $lookup->SmartALookup($domain, "A");

	if(!$result){
		return array();
	}else{
        return array($result);
    }

	$temp = array();
	return $temp;
}

function text_sort($a, $b){
    return $a > $b;
}

function checkBlacklists($ip){
    $temp = array();
    $blacklists = array("b.barracudacentral.org",
                        "ubl.unsubscore.com",
                        "dnsbl.sorbs.net",
                        "bl.spamcop.net",
                        "bl.spamcannibal.org",
                        "zen.spamhaus.org",
                        "dnsbl-1.uceprotect.net",
                        "dnsbl-2.uceprotect.net",
                        "dnsbl-3.uceprotect.net");
    usort($blacklists, "text_sort");
    foreach($blacklists as $bl){
        $query = sprintf("%s.%s", $ip, $bl);
        $result = getRecords($query);
        if(!$result){
            array_push($temp, array('RBL' => $bl, 'Status' => '0'));
        }else{
            array_push($temp, array('RBL' => $bl, 'Status' => $result[0]));
        }
    }

    echo json_encode($temp, JSON_PRETTY_PRINT);
}

$ip = $_GET['ip'];
$pieces = explode('.', $ip);
$revpices = array_reverse($pieces);
$ip = sprintf("%d.%d.%d.%d", $revpices[0], $revpices[1], $revpices[2], $revpices[3]);
checkBlacklists($ip);
?>
