<?php

function getScreenshot($domainName)
{
    printf("<p class='contentTitle'>Render of %s</p><br />", $domainName);

    $call = sprintf("phantomjs ../phantom/render.js %s", $domainName);

    exec($call, $output, $sig);

    if($sig == 1){
        printf("%s", "Web page unavailable.");
    }
    else{
        printf("<img src='screenshot/%s.png'></img>", $domainName);
    }
}

if(isset($_GET['domain']))
{
    $domain = $_GET['domain'];
}
else
{
    $domain = "derekholio.com";
}

$filename = "../screenshot/$domain.png";
if(file_exists($filename))
{
    $filetime = filemtime($filename);
	
    if($filetime-time() < -1*60*60)
    {
        getScreenshot($domain);
    }
    else
    {
        printf("<p class='contentTitle'>Render of %s (%s)</p>", $domain, gmdate("F d, Y H:i:s e", $filetime));
        printf("<img src='screenshot/%s.png?purge=%.0f'></img><br /><br />", $domain, time());
        printf("<p>Snapshots are cached for 1 hour.</p>");
    }
}
else
{
    getScreenshot($domain);
}




?>
