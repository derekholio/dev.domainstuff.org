<?php

if(strcmp($_SERVER['HTTP_HOST'], "dev.domainstuff.org") == 0){
	ERROR_REPORTING(E_ALL & ~E_DEPRECATED);
}else{
	ERROR_REPORTING(~E_ALL);
}

if(isset($_GET['cmd']) && strcmp($_GET['cmd'],"GetGeoIP") != 0)
{
    die('');
}

if(isset($_GET['domain']))
{
    $domain = $_GET['domain'];

    if(strcmp($domain, "") == 0)
    {
        die();
    }

    if(!filter_var($domain, FILTER_VALIDATE_IP))
    {
        $lookup = file_get_contents("http://".$_SERVER['HTTP_HOST']."/php/dnsrecords.php/?domain=".$domain);
        //print_r($lookup); echo "<br /><br />";
        $lookup_decode = json_decode($lookup);
		
		if(!$lookup_decode){
			die("Estimated location: Failed to get geo location for $domain");
		}
		
		try{
			if(array_key_exists("error", $lookup_decode)){
				die("Estimated location: Failed to get geo location for $domain");
			}
			
			if(array_key_exists('A', $lookup_decode)){
				$ip = $lookup_decode->A[0]->A;
			}else{
				die("Estimated location: Invalid domain ($domain)");
			}
			

			if($ip && !count($ip) > 0)
			{
				die("Estimated location: Invalid domain ($domain)");
			}
			$domain = $ip;
			//die("Reached here");
			//$domain = $ip[0]['ip'];
		}catch(Exception $e){
			die("Estimated location: Failed to get geo location for $domain");
		}
    }
}
else
{
    die('Estimated location: Invalid domain name.');
}

$filename = "geoloc-cache/$domain.cache";
if(!file_exists($filename))
{
    $call = sprintf('phantomjs ../phantom/iploc.js %s', $domain);

    exec($call, $output, $sig);

    if($sig == 1)
    {
        print "Failed to pull location";
    }

    $file = fopen($filename, "w");
    fwrite($file, $output[0]);
    fclose($file);
    printf("%s", $output[0]);
}
else
{
    $file = fopen($filename, "r");
    $loc = fread($file, filesize($filename));
    printf("%s", $loc);
}


?>
