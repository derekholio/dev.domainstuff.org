<?php

if(strcmp($_SERVER['HTTP_HOST'], "dev.domainstuff.org") == 0){
	ERROR_REPORTING(E_ALL & ~E_DEPRECATED);
}else{
	ERROR_REPORTING(~E_ALL);
}

//http://www.purplepixie.org/phpdns/

function store_cache($domain, $cacheObject){
	$cache_loc = "./dns-cache/";
	$cache_file = $cache_loc.$domain.".cache";
	$file = fopen($cache_file, "w");
	fwrite($file, serialize($cacheObject));
	fclose($file);
}

function get_cache($domain){
	$cache_loc = "./dns-cache/";
	$cache_file = $cache_loc.$domain.".cache";
	if(file_exists($cache_file)){
		$modtime = filemtime($cache_file);
		$secs = time()-$modtime;
		if($secs > 60*5){
			return false;
		}else{
			$file = fopen($cache_file, "r");
			$contents = unserialize(fread($file, filesize($cache_file)));
			return $contents;
		}
	}else{
		return false;
	}
}

if(!isset($_GET['domain'])){
	$res = array('Error' => 'Please provide a domain name.');
	die(json_encode($res));
}else{
	$domain = $_GET['domain'];
}

require('phpdns-1.05/dns.inc.php');

$result_array = array();

function sort_mx($a, $b){
	if($a['Priority'] > $b['Priority']){
		return 1;
	}else if($a['Priority'] < $b['Priority']){
		return -1;
	}else{
		return 0;
	}
}

function getRecords($domain, $type){
	$server = "208.88.77.77";
	$lookup = new DNSQuery($server);
	$lookup->timeout = 3;
	$result = $lookup->Query($domain, $type);

	if(!$result){
		return array();
	}
	$result_count=$result->count;

	$temp = array();

	if($type == "MX"){
		for($i=0; $i<$result_count; $i++){
			$record = $result->results[$i]->data;
			$pri = $result->results[$i]->extras['level'];
			array_push($temp, array('Record' => $record, 'Priority' => $pri));
		}
		usort($temp, "sort_mx");
	}else if($type == "SOA"){
		for($i=0; $i<$result_count; $i++){
			$soa_results = $result->results[$i]->extras;
			$record = $result->results[$i]->data;
			$serial = $soa_results['serial'];
			$refresh = $soa_results['refresh'];
			$retry = $soa_results['retry'];
			$expiry = $soa_results['expiry'];
			$minttl = $soa_results['minttl'];
			$responsible = $soa_results['responsible'];
			array_push($temp,
			array('Record' => $record,
				'Serial' => $serial,
				'Refresh' => $refresh,
				'Retry' => $retry,
				'Expiry' => $expiry,
				'Minimum TTL' => $minttl,
				'Responsible' => $responsible
			));
		}
	}else{
		$temp = array();
		for($i=0; $i<$result_count; $i++){
			$record = $result->results[$i]->data;
			array_push($temp, $record);
		}
	}

	return $temp;
}

function getIPv6($domain, &$res){
	$result = dns_get_record($domain, DNS_AAAA);
	if($result){
		$temp = array();
		for($i = 0; $i < count($result); $i++){
			array_push($temp, $result[$i]['ipv6']);
		}
		$res['AAAA'] = $temp;
	}

}

function getARecords($domain, &$res){
	$ares = array();

	$result = getRecords($domain, "A");
	foreach($result as $ip){
		$ptr = getRecords($ip, "PTR");
		if(count($ptr) > 0){
			array_push($ares, array('A' => $ip, 'PTR' => $ptr[0]));
		}else{
			array_push($ares, array('A' => $ip, 'PTR' => 'None'));
		}
	}
	if($result){
		$res['A'] = $ares;
	}
}

function getNSRecords($domain, &$res){
	$result = getRecords($domain, "NS");

	$temp = array();
	for($i = 0; $i<count($result); $i++){
		$ns = $result[$i];
		$a = getRecords($ns, "A");
		foreach($a as $ip){
			array_push($temp, array('A' => $ip, 'NS' => $ns));
		}
	}

	if($result){
		$res['NS'] = $temp;
	}
}

function getMXRecords($domain, &$res){
	$result = getRecords($domain, "MX");

	for($i = 0; $i<count($result); $i++){
		$mx = $result[$i]['Record'];
		$a = getRecords($mx, "A");
		foreach($a as $ip){
			$result[$i]['A'] = $ip;
			$ptr = getRecords($ip, "PTR");
			if(count($ptr)>0){
				$result[$i]['PTR'] = $ptr[0];
			}else{
				$result[$i]['PTR'] = 'None';
			}
		}
	}
	if($result){
		$res['MX'] = $result;
	}
}

function getTXTRecords($domain, &$res){
	$result = getRecords($domain, "TXT");
	if($result){
		$res['TXT'] = $result;
	}
}

function getSOARecord($domain, &$res){
	$result = getRecords($domain, "SOA");
	if($result){
		$res['SOA'] = $result;
	}
}

if(filter_var($domain, FILTER_VALIDATE_IP)){
	$cache = get_cache($domain);
	if(!$cache || (isset($_GET['nocache']) && $_GET['nocache'] == 1)){
		$results = getRecords($domain, "PTR");
		if(!$results){
			$result_array = array('IP' => $domain, 'PTR' => 'None');
		}else{
			$result_array = array('IP' => $domain, 'PTR' => $results[0]);
		}
		store_cache($domain, $result_array);
		echo json_encode($result_array);
	}else{
		echo json_encode($cache);
	}
}else{
	$cache = get_cache($domain);
	if(!$cache || (isset($_GET['nocache']) && $_GET['nocache'] == 1)){
		getARecords($domain, $result_array);
		getIPv6($domain, $result_array);
		getNSRecords($domain, $result_array);
		getMXRecords($domain, $result_array);
		getTXTRecords($domain, $result_array);
		getSOARecord($domain, $result_array);

		$empty = true;
		for($i = 0; $i < count($result_array); $i++){
			if(!empty($result_array)){
				$empty = false;
			}
		}

		if($empty){
			$result_array['error'] = array("No DNS records were found for $domain");
			store_cache($domain, $result_array);
			echo json_encode($result_array);
		}else{
			store_cache($domain, $result_array);
			echo json_encode($result_array);
			//echo json_encode($result_array);
		}
	}else{
		echo json_encode($cache);
	}
}
?>
