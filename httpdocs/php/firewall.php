<?php

exec('cmd /c "..\cleanup\cleanup.bat"', $out);//clean up cache with requests to firewall

$domain = $_GET['domain'] ?? die(json_encode(array('error' => 'No domain provided.')));

$ports = array();
$results = array();

$file = fopen('ports.resources', 'r');
if($file){
    while(($buffer = fgets($file, 4096)) !== false){
        $arr = explode(',', $buffer);
        if(count($arr)==2){
            $port = $arr[0];
            $service = $arr[1];
            $service = trim(preg_replace('/\s\s+/', '', $service));//Replace new lines
            array_push($ports, array('Port' => $port, 'Service' => $service));
        }
    }
    fclose($file);

    foreach($ports as $sList){
        $port = $sList['Port'];
        $service = $sList['Service'];
        $connection = @fsockopen($domain, $port, $errno, $errstr, .5);
        if(is_resource($connection)){
            $status = 'Open';
        }else{
            $status = '';
        }
        array_push($results, array('Port' => $port, 'Service' => $service, 'Status' => $status));
    }
}else{
    die(json_encode(array('error' => 'Failed to load the port resources')));
}

echo json_encode($results);
 ?>
