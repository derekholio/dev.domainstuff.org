<html>
<body>
  <table border=1>
<?php
ini_set('display_errors', 1);

function queryDB($query)
{
    $dbCreds = [
      "host" => "localhost",
      "username" => "supportcenter",
      "password" => "jnRg9CdEZtMe",
      "database" => "supportcentersite"
    ];

    $db = mysqli_connect($dbCreds['host'], $dbCreds['username'], $dbCreds['password'], $dbCreds['database']) or die($db->connect_error);
    $result = mysqli_query($db, $query) or die("Failed to query");
    mysqli_close($db) or die("Failed to close");
    return $result;
}

if(isset($_GET['ChatId']))//Retrieve and display chat records
{
  $ChatId = $_GET['ChatId'];

  $query = "SELECT Date,ChatMessage FROM ChatLog WHERE ChatId = '$ChatId' ORDER BY IndexNum ASC;";

  $result = queryDB($query);
  if(mysqli_num_rows($result) < 1)
  {
    print "ChatID not found.";
    exit(1);
  }
  print "<thead><tr><td>Time</th><th>Person</th><th>Message</th></tr></thead>";
  while($row = $result->fetch_row())
  {
    preg_match('/(.*?):(.*)/', $row[1], $match);
    print "<tr><td>".$row[0]."<td>".$match[1]."</td><td>".$match[2]."</td></tr>";
  }
}
else//Retrieve and display records
{
  $query = "SELECT DISTINCT ChatId from ChatLog ORDER BY ChatId ASC;";//Retrieve ChatId in asc order

  $chatIds = [];
  $chatIdResult = queryDB($query);//retrieve ChatIds
  while($row = $chatIdResult->fetch_row())
  {
      array_push($chatIds, $row[0]);//Add ChatId to $chatIds
  }

  print "<thead><tr><th>ChatId</th><th>Last Activity</th><th>Messages</th></tr></thead>";
  foreach($chatIds as $chatId)
  {
      $query = "SELECT COUNT(*) FROM ChatLog WHERE ChatId = '$chatId';";
      $messNoResult = queryDB($query);//Retrieve number of messages
      while($row = $messNoResult->fetch_row())
      {
          $numMess = $row[0];

          $query = "SELECT Date FROM ChatLog WHERE ChatId = '$chatId' ORDER BY Date DESC LIMIT 1;";
          $dateResult = queryDB($query);//Retrieve the last activity date
          if($numMess > 0)
          {
            $dateRow = $dateResult->fetch_row();
            $lastDate = $dateRow[0];
          }
          else
          {
            $lastDate = "No activity";
          }

          print "<tr><td><a href=\"?ChatId=$chatId\" target=\"_blank\">$chatId</a></td><td>$lastDate</td><td>$numMess</td></tr>";
      }
  }
}

?>
</table>
</body>
</html>
