<?php

function store_cache($domain, $cacheObject){
	$cache_loc = "./ssl-cache/";
	$cache_file = $cache_loc.$domain.".cache";
	$file = fopen($cache_file, "w");
	fwrite($file, serialize($cacheObject));
	fclose($file);
}

function get_cache($domain){
	$cache_loc = "./ssl-cache/";
	$cache_file = $cache_loc.$domain.".cache";
	if(file_exists($cache_file)){
		$modtime = filemtime($cache_file);
		$secs = time()-$modtime;
		if($secs > 60*5){
			return false;
		}else{
			$file = fopen($cache_file, "r");
			$contents = unserialize(fread($file, filesize($cache_file)));
			return $contents;
		}
	}else{
		return false;
	}
}

if(!isset($_GET['domain'])){
    die("Invalid parameters.");
}

require('phpdns-1.05/dns.inc.php');
function getARecord($domain){
	$server = "208.88.77.77";
	$lookup = new DNSQuery($server);
	$lookup->timeout = 3;
	$result = $lookup->SmartALookup($domain, "A");
    return $result;
}

$domain = $_GET['domain'];
$ip = getARecord($domain);

$cache = get_cache($domain);
if(!$cache){
    $url = sprintf("https://ssldecoder.org/json.php?host=%s:%s", $domain, $ip);
    $json_call = file_get_contents($url);
    store_cache($domain, $json_call);
    
    echo $json_call;
}else{
    echo $cache;
}



?>
