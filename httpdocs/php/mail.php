<?php
require_once('PHPMailer-5.2.10/PHPMailerAutoload.php');

ini_set("display_errors", 1);

function logtofile($message)
{
	if(!file_exists("mail.log"))
	{
		$file = fopen("mail.log", "w");
		fclose($file);
	}
	
	$file = fopen("mail.log", "a");
	fwrite($file, $message."\n");
	fclose($file);
}



if(!ISSET($_GET['g-recaptcha-response']))
{
	print "No recaptcha code provided.";
	exit();
}
else
{
	$url = "https://www.google.com/recaptcha/api/siteverify";
	$data = array('secret' => '6LcAkgcTAAAAACWzHuMRGmpwhVsWCs0xz4rqO1Go', 'response' => $_GET['g-recaptcha-response']);
	$options = array(
		'http' => array(
			'header' => 'application/json', 
			'method' => 'POST', 
			'content' => http_build_query($data),
		),
	);
	
	$context = stream_context_create($options);
	$result = file_get_contents($url, false, $context);
	
	$response = json_decode($result);
	if(!$response->{'success'})
	{
		print "Google recaptcha failed.";
		exit();
	}
}

/*
$from = $_get['email'];
$subject = $_get['subject'];
$to = "ryan.peterson@managed.com; derek.sims@managed.com";
$message = $_get['message'];

$header = "From: ".$from."";

mail($to, $subject, $message, $headers);
*/
ini_set("SMTP", "localhost");
//ini_set("sendmail_from", "ryan.peterson@managed.com");

/*
	127.0.0.1 SMTP bypass
	user: no-reply@supportcenter.perigonnetworks.com
	pass: HPXbvjrt
*/

$subject = 'Feedback: '.$_GET['emailSubject'];
$message = "Message: ".$_GET['emailMessage'];
$submitFrom = $_GET['emailFrom'];
$domain = $_GET['query_domain'];
$to = array(
"Ryan Peterson" => "ryan.peterson@managed.com",
"Derek Sims" => "derek.sims@managed.com",
);

$httpHost = $_SERVER['HTTP_HOST'];

$from     = 'no-reply@'.str_replace('www.', '', $_SERVER['SERVER_NAME']);
if(isset($_SERVER["HTTP_CF_CONNECTING_IP"])) 
{ 
	$userip = sprintf("<a href='https://%s/%s'>%s</a>", $httpHost, $_SERVER["HTTP_CF_CONNECTING_IP"], $_SERVER["HTTP_CF_CONNECTING_IP"]); 
}
elseif(isset($_SERVER['REMOTE_ADDR']))
{
	$userip = sprintf("<a href='https://%s/%s'>%s</a>", $httpHost, $_SERVER['REMOTE_ADDR'], $_SERVER['REMOTE_ADDR']);
}
else 
{ 
	$userip = "No IP found."; 
}

$message = sprintf("%s<br /><br />", $message);
$message .= sprintf("From: %s<br />", $submitFrom);
$message .= sprintf("Domain: %s<br />", $domain);
$message .= sprintf("User IP: %s", $userip);


//$message = "\n\n".$message."\n\nFrom: ".$submitFrom."\nDomain: ".$domain."\nUser IP: ".$userip;

$mail = new PHPMailer();

$mail->IsSMTP();

$mail->CharSet = 'UTF-8';

$mail->SMTPAuth = true;
$mail->Host = "relay.us.powerdnn.com";
$mail->Port = 25;
$mail->Username = "supportcenter@relay.powerdnn.com";
$mail->Password = "y6SpE6AC";

$mail->SetFrom($from, 'DomainStuff Feedback');

foreach($to as $toName => $toAddr)
{
	logtofile($toName);
	logtofile($toAddr);
	$mail->AddAddress($toAddr, $toName);
}

$mail->Subject = $subject;
$mail->Body = $message;
$mail->IsHTML(true);

logtofile($subject);
logtofile($message);

if(!$mail->Send())
{
	echo '<pre>';
	echo "Failed to send: " . $mail->ErrorInfo;
	echo '</pre>';
	logtofile($mail->ErrorInfo);
	logtofile(var_dump($mail));
}
else
{
	echo "Message sent!";
	logtofile("Message sent.\n");
}

?>
