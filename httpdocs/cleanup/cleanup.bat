@echo OFF
forfiles -p ".." -s -m *.cache /D -1 /C "cmd /c del @path" 2>nul
forfiles -p "..\php\geoloc-cache" -s -m *.cache /D -1 /C "cmd /c del @path" 2>nul
forfiles -p "..\php\ssl-cache" -s -m *.cache /D -1 /C "cmd /c del @path" 2>nul
forfiles -p "..\screenshot" -s -m *.png /D -1 /C "cmd /c del @path" 2>nul