import cgi

import requests

print "Content-Type: text/html\r\n"


def get_redirect_status(host, attempts=0):
    local_attempts = attempts
    if local_attempts > 10:
        print '<div class="containerContent bad">Too many redirects. Aborting.<br /></div>'
    else:
        try:
            r = requests.request("GET", host, headers={'User-Agent': 'Managed.com Redirect Checker'},
                                 allow_redirects=False, timeout=10)
            local_attempts += 1
            if r.status_code == 301 or r.status_code == 302:
                for header in r.headers:
                    if header == "location":
                        print '<div class="containerContent">Redirected to %s (%d %s)<br /></div>' % (
                            r.headers["location"], r.status_code, r.reason)
                        get_redirect_status(r.headers["location"], local_attempts)
            elif r.status_code >= 400:
                print '<div class="containerContent bad">End at %s (%d %s)<br /></div>' % (
                    host, r.status_code, r.reason)
            else:
                print '<div class="containerContent good">Ended at %s (%d %s)<br /></div>' % (
                    host, r.status_code, r.reason)
        except requests.exceptions.ReadTimeout:
            print '<div class="containerContent bad">Timeout exceeded; Aborting redirect checks.</div>'
        except requests.exceptions.ConnectionError:
            print '<div class="containerContent bad">Failed to connect to %s.</div>' % host
        except requests.exceptions.InvalidURL:
            print '<div class="containerContent bad">Bad URL: %s.</div>' % host
        except:
            print '<div class="containerContent bad">An exception occurred while browsing %s.</div>' % host


def main():
    form = cgi.FieldStorage()
    if form.getvalue('cmd') == 'CheckRedirect':
        query_domain = form.getvalue('domain')
        print '<div class="containerContent">Starting at http://%s<br /></div>' % query_domain
        get_redirect_status('http://' + query_domain)
    else:
        query_domain = 'managed.com'
        get_redirect_status('http://' + query_domain)


if __name__ == '__main__':
    main()
else:
    pass
