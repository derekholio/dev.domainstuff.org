import dns.resolver


class DNSDriver:
    def __init__(self, domain):
        self.domain = domain
        self.last_error = None

    def query_dns(self, query_type):
        if query_type == "PTR":
            try:
                domain = dns.reversename.from_address(self.domain)
            except dns.exception.SyntaxError:
                return -1
        else:
            domain = self.domain
        try:
            resolver = dns.resolver.Resolver()
            resolver.timeout = 4
            resolver.lifetime = 4
            answers = resolver.query(domain, query_type)
            return answers
        except dns.resolver.Timeout:
            self.last_error = "No record (timeout)"
            return -1
        except dns.resolver.NXDOMAIN:
            self.last_error = "Non existant domain"
            return -2
        except dns.name.LabelTooLong:
            self.last_error = "Domain name is too long"
            return -3
        except dns.resolver.NoAnswer:
            self.last_error = "No record"
            return -4
        except dns.resolver.NoNameservers:
            self.last_error = "No name servers available to query"
            return -5
        except dns.name.EmptyLabel:
            self.last_error = "Empty domain name given"
            return -6

    def get_last_error(self):
        return self.last_error


def main():
    dns_check = DNSDriver('derekholio.com')
    answers = dns_check.query_dns('SOA')
    if answers < 0:
        print dns_check.get_last_error()
    else:
        for answer in answers:
            print answer.serial


if __name__ == '__main__':
    main()
else:
    pass
