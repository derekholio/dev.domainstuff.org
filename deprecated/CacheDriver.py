import pickle
import os
from DNSDriver import DNSDriver
import datetime
import FileUtils


class CacheDriver:
    def __init__(self, domain):
        cur_path = os.path.dirname(os.path.realpath(__file__))
        self.cache_dir = cur_path + '\\cache\\' + domain + '.cache'

        self.cache_time = None
        self.domain = domain
        self.dns_content = None
        self.rdns_content = None
        self.blacklist_content = None
        self.serial = None

        soa_checker = DNSDriver(domain)
        answers = soa_checker.query_dns('SOA')
        if answers < 0:  # Didn't get an SOA back, skip looking for cache
            self.has_cache = False
        else:  # Get an SOA. Check it against the stored SOA
            serial = answers[0].serial
            self.serial = serial
            if os.path.isfile(self.cache_dir):
                cache_check = pickle.load(open(self.cache_dir, "rb"))
                FileUtils.write_debug("Cached serial: %s" % cache_check.serial)
                FileUtils.write_debug("Current serial: %s" % serial)
                FileUtils.write_debug("Cached time: %s" % cache_check.cache_time)
                FileUtils.write_debug("Current time: %s" % datetime.datetime.now())
                FileUtils.write_debug(
                    "Cache expires at: %s" % str(cache_check.cache_time + datetime.timedelta(minutes=5)))
                if (cache_check.serial == serial) and not \
                        (cache_check.cache_time < datetime.datetime.now() - datetime.timedelta(minutes=5)):
                    self.domain = cache_check.domain
                    self.dns_content = cache_check.dns_content
                    self.rdns_content = cache_check.rdns_content
                    self.blacklist_content = cache_check.blacklist_content
                    self.has_cache = True
                else:
                    self.has_cache = False
            else:
                self.has_cache = False

    def has_cache(self):
        return self.has_cache

    def set_dns_content(self, dns_content):
        self.dns_content = dns_content

    def get_dns_content(self):
        return self.dns_content

    def set_rdns_content(self, rdns_content):
        self.rdns_content = rdns_content

    def get_rdns_content(self):
        return self.rdns_content

    def set_blacklist_content(self, blacklist_content):
        self.blacklist_content = blacklist_content

    def get_blacklist_content(self):
        return self.blacklist_content

    def store_cache(self):
        self.cache_time = datetime.datetime.now()
        try:
            pickle.dump(self, open(self.cache_dir, "wb"))
        except:  # Failed to pickle :(
            pass


def main():
    cache_driver = CacheDriver('derekholio.com')
    print cache_driver.get_blacklist_content()


if __name__ == '__main__':
    main()
else:
    pass
