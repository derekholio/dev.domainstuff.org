import cgi
import os
import re
from socket import AF_INET, SOCK_STREAM, socket

from DNSDriver import DNSDriver

'''
To do: Allow input for custom ports?
'''

print "Content-Type: text/html\r\n"


def initialize_ports():
    ports_dict = {}
    cur_path = os.path.dirname(os.path.realpath(__file__))
    ports = open(cur_path + '\\resources\\ports.txt', 'r')
    for line in ports:
        m = re.search('(\d+)\s(.*)', line)
        if m is not None:
            port = m.group(1)
            service = m.group(2)
            ports_dict[port] = service
    ports.close()
    return ports_dict


def get_open_ports(domain, is_ip=False):
    common_ports = initialize_ports()
    ports = sorted(common_ports.keys(), key=int)
    if is_ip:
        ip = domain
        for port in ports:
            sock = socket(AF_INET, SOCK_STREAM)
            sock.settimeout(.5)
            result = sock.connect_ex((ip, int(port)))
            if result == 0:
                print '<div class="containerContent good"><b>%s (%s)</b>: %s</div>' \
                      % (port, common_ports[port], "OPEN")
            else:
                print '<div class="containerContent bad"><b>%s (%s)</b>: %s</div>' \
                      % (port, common_ports[port], "CLOSED")
    else:
        dns_checker = DNSDriver(domain)
        a_query = dns_checker.query_dns('A')
        if a_query < 0:
            print '<div class="containerContent bad">Failed to retrieve DNS for %s: %s</div>' % (
                domain, dns_checker.get_last_error())
        else:
            ip = str(a_query[0])
            for port in ports:
                sock = socket(AF_INET, SOCK_STREAM)
                sock.settimeout(.5)
                result = sock.connect_ex((ip, int(port)))
                if result == 0:
                    print '<div class="containerContent good"><b>%s (%s)</b>: %s</div>' \
                          % (port, common_ports[port], "OPEN")
                else:
                    print '<div class="containerContent bad"><b>%s (%s)</b>: %s</div>' \
                          % (port, common_ports[port], "CLOSED")


def main():
    form = cgi.FieldStorage()
    if form.getvalue('cmd') == 'GetPorts':
        query_domain = form.getvalue('domain')
        m = re.search('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', query_domain)
        if m is None:
            is_ip = False
        else:
            is_ip = True
        get_open_ports(query_domain, is_ip)
    else:
        query_domain = 'derekholio.com'
        get_open_ports(query_domain)


if __name__ == '__main__':
    main()
else:
    pass
