import re

from DNSDriver import DNSDriver


class BlacklistDriver:
	def __init__(self, domain):
		self.domain = domain
		self.is_ip = None
		m = re.search('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})', domain)
		if m is None:
			self.is_ip = False
		else:
			self.is_ip = True
		self.revIP = reverse_ip(domain)
		if self.revIP is None:
			raise Exception('Invalid domain')

	def check_barracuda(self):
		"""
		Returns all results if found, otherwise returns None.
		An IP of 127.0.0.2 indicates it is blacklisted.
		"""

		url = "b.barracudacentral.org"
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Returns none on any error
			return None

	def check_lashback(self):
		"""
		Returns all results if found, otherwise returns None.
		An IP of 127.0.0.2 indicates it is blacklisted.
		"""

		url = 'ubl.unsubscore.com'
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Return none on any error
			return None

	def check_sorbs(self):
		url = 'dnsbl.sorbs.net'
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Return none on any error
			return None

	def check_spamcop(self):
		"""
		Returns all results if found, otherwise returns None.
		An IP of 127.0.0.2 indicates it is blacklisted.
		"""

		url = "bl.spamcop.net"
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Returns none on any error
			return None

	def check_spamcannibal(self):
		"""
		Returns all results if found, otherwise returns None.
		An IP of 127.0.0.2 indicates it is blacklisted.
		"""

		url = "bl.spamcannibal.org"
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Returns none on any error
			return None

	def check_spamhaus(self):
		"""

		:return:
		"""

		url = "zen.spamhaus.org"
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Returns none on any error
			return None

	def check_uceprotectlevel1(self):
		url = "dnsbl-1.uceprotect.net"
		query_url = "%s.%s" % (self.revIP, url)
		try:
			dns_checker = DNSDriver(query_url)
			answers = dns_checker.query_dns('A')
			if answers < 0:
				return None
			else:
				return answers
		except:	 # Returns none on any error
			return None

	def check_blacklists(self):
		blacklist_functions_dict = {'Barracuda': self.check_barracuda, 'LashBack': self.check_lashback,
									'SORBS': self.check_sorbs, 'SpamCannibal': self.check_spamcannibal,
									'SpamCop': self.check_spamcop,
									'SpamHaus ZEN': self.check_spamhaus, 'UCEPROTECTL1': self.check_uceprotectlevel1}
		good_response = '<div class="containerContent good"><div class="mono">' \
						'<span><b>%s: </b></span></div>%s is not listed!</div>'
		bad_response = '<div class="containerContent bad"><div class="mono">' \
					   '<span><b>%s: </b></span></div>%s is listed! %s</div>'
		blacklists_dict = {}
		blacklist_dict = blacklist_functions_dict.keys()
		blacklist_dict.sort()
		for blacklist in blacklist_dict:
			answers = blacklist_functions_dict[blacklist]()
			if answers is None:
				blacklists_dict[blacklist] = good_response % (blacklist, self.domain)
			else:
				temp = ""
				answers = list(answers)
				answers.sort()
				for answer in answers:
					temp += "(%s) " % str(answer)
				blacklists_dict[blacklist] = bad_response % (blacklist, self.domain, temp)
		return blacklists_dict


def reverse_ip(domain):
	m = re.search('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})', domain)
	if m is not None:
		m = re.search('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})', domain)
		if m is None:
			return None
		else:
			first, second, third, fourth = (m.group(1), m.group(2), m.group(3), m.group(4))
			rev_ip = "%s.%s.%s.%s" % (fourth, third, second, first)
			return rev_ip
	else:
		dns_checker = DNSDriver(domain)
		a_query = dns_checker.query_dns('A')
		if a_query < 0:
			return None
		else:
			ip = str(a_query[0])
			m = re.search('(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})', ip)
			if m is None:
				return None
			else:
				first, second, third, fourth = (m.group(1), m.group(2), m.group(3), m.group(4))
				rev_ip = "%s.%s.%s.%s" % (fourth, third, second, first)
				return rev_ip


def main():
	print ""


if __name__ == '__main__':
	try:
		BlacklistChecker = BlacklistDriver('70.34.39.56')
		print BlacklistChecker.check_barracuda()
		print BlacklistChecker.check_lashback()
		print BlacklistChecker.check_spamcannibal()
		print BlacklistChecker.check_spamcop()
		print BlacklistChecker.check_spamhaus()
	except Exception:
		print 'Invalid domain'
else:
	pass
